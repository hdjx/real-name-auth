package com.asurplus;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 项目启动类
 *
 * @Author admin
 */
@Slf4j
@SpringBootApplication
public class AsurplusApplication {

    public static void main(String[] args) {
        SpringApplication.run(AsurplusApplication.class, args);
    }
}
