package com.asurplus.common.realname.api;

/**
 * 上报数据对象
 */
public class ReportData {
    /**
     * 在批量模式中标识一条行为数据，取值范围 1-128
     * 发送时注入
     */
    private int no;
    /**
     * 上下线会话编号
     * 上线时标记
     */
    private String si;
    /**
     * 游戏用户行为类型
     * 0：下线
     * 1：上线
     */
    private int bt;
    /**
     * 行为发生时间戳，单位秒
     */
    private long ot;
    /**
     * 用户行为数据上报类型
     * 0：已认证通过用户
     * 2：游客用户
     */
    private int ct;
    /**
     * 32位
     * 游客模式设备标识，由游戏运营单位生成，游客用户下必填
     */
    private String di;
    /**
     * 已通过实名认证用户的唯一标识，已认证通过用户必填
     */
    private String pi;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getSi() {
        return si;
    }

    public void setSi(String si) {
        this.si = si;
    }

    public int getBt() {
        return bt;
    }

    public void setBt(int bt) {
        this.bt = bt;
    }

    public long getOt() {
        return ot;
    }

    public void setOt(long ot) {
        this.ot = ot;
    }

    public int getCt() {
        return ct;
    }

    public void setCt(int ct) {
        this.ct = ct;
    }

    public String getDi() {
        return di;
    }

    public void setDi(String di) {
        this.di = di;
    }

    public String getPi() {
        return pi;
    }

    public void setPi(String pi) {
        this.pi = pi;
    }
}
