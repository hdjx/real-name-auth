package com.asurplus.common.realname.util;

import org.nutz.json.Json;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;

/**
 * 接口加密
 */
public final class ParamSecretUtil {

    private ParamSecretUtil() {
    }

    /**
     * 对请求报文体数据进行AES-128/GCM + BASE64算法加密
     *
     * @param body
     * @param secretKey
     * @return
     */
    public static String encodeBody(String body, String secretKey) {
        return AESUtil.Encrypt(body, secretKey);
    }

    /**
     * 生成签名,有body的情况
     *
     * @param sysPrams  系统参数
     * @param body      业务参数-请求体
     * @param secretKey 访问密钥
     * @return
     */
    public static String buildSign(Map<String, String> sysPrams, String body, String secretKey) {
        /*
         * 1、 将除去 sign 的系统参数和除去请求体外的业务参数，
         * 根据参数的 key 进行 字典排序，并按照 Key-Value 的格式拼接成一个字符串。
         * 将请求体中的参数 拼接在字符串最后。
         */
        ArrayList<String> keys = new ArrayList<>();
        keys.addAll(sysPrams.keySet());
        keys.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });
        StringBuilder sb = new StringBuilder();
        sb.append(secretKey);
        for (String key : keys) {
            if (key.equals("Content-Type")) continue;
            String value = sysPrams.get(key);
            sb.append(key).append(value);
        }
        sb.append(body);
        return SHAUtil.SHA256(sb.toString());
    }

    /**
     * 生成签名,没body的情况
     *
     * @param sysPrams
     * @param extParams
     * @param secretKey
     * @return
     */
    public static String buildSign(Map<String, String> sysPrams, Map<String, Object> extParams, String secretKey) {
        /*
         * 1、 将除去 sign 的系统参数和除去请求体外的业务参数，
         * 根据参数的 key 进行 字典排序，并按照 Key-Value 的格式拼接成一个字符串。
         * 将请求体中的参数 拼接在字符串最后。
         */
        ArrayList<String> keys = new ArrayList<>();
        keys.addAll(sysPrams.keySet());
        keys.addAll(extParams.keySet());
        keys.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });
        StringBuilder sb = new StringBuilder();
        sb.append(secretKey);
        for (String key : keys) {
            if (key.equals("Content-Type")) continue;
            String value = sysPrams.get(key);
            if (value == null) {
                Object buzValue = extParams.get(key);
                if (buzValue != null) {
                    if (buzValue instanceof String) {
                        value = (String) buzValue;
                    } else {
                        value = Json.toJson(buzValue);
                    }
                }
            }
            sb.append(key).append(value);
        }
        return SHAUtil.SHA256(sb.toString());
    }
}
